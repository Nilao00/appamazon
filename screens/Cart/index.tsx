import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import Header from '@/components/Header';
import ButtonView from '@/components/Buttons';
import Card from '../Card';
import {
    Container,
    ItemsProduct,
    ScrollViewItems,
    FooterInfomation,
    TextFooter,
    Title
} from './styles';
import RadiosButton from '@/components/RadiosButton';


const Cart = () => {
    const [showSearch, setShowSearch] = useState(false);
    return (
        <Container>
            <StatusBar backgroundColor={"#bcf9fa"} />
            <Header showSearch={showSearch} setShowSearch={setShowSearch} />
            <Title>Mensagem sobre produto no carrinho</Title>
            <Title style={{ marginTop: 20 }}>Subtotal : 10.999,11</Title>
            <ButtonView title="Fechar pedido(4 itens)" borderRadius='15px' color="#fff" style={{
                background: "#ffd700",
                padding: "5px"
            }} />
            <RadiosButton color="blue" colorSelected='red' size={40} />
            <FooterInfomation>
                <TextFooter>
                    O App day comeca em 32:42:40
                </TextFooter>
            </FooterInfomation>
        </Container>
    );
}

export default Cart;