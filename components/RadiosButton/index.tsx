import React from 'react';
import RadioForm from 'react-native-simple-radio-button';

import { Container } from './styles';

interface propsRadios {
    color: string;
    size: number;
    colorSelected: string;
}

const RadiosButton = ({ color, colorSelected, size }: propsRadios) => {
    return (
        <Container>
            <RadioForm
                style={{ width: size }}
                buttonColor={color}
                initial={0}
                animation={false}
            />
        </Container>
    )
}

export default RadiosButton;